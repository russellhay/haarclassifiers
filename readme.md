Haar Classifier Collection
==========================

This is a collection of haar classifiers for use with OpenCV.

Each directory is organized with the following directories and files:

[name].xml - The classifier to load into OpenCV
samples.vec - The combined training data
positives/* - The positive images, cropped to just what's important
negatives/* - A random collection of scenes that do not include the object to be detected
samples/* - A combination of positives and negatives to increase the training sample set

The following classifiers currently exist or are in development:

dog - Used to detect if there is a dog in the picture for IsThisADogOrNot.com
